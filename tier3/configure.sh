#!/bin/bash

set -e

rm -f $SNAP_DATA/configured

declare -A CONFIG

require() {
  x="$(snapctl get $1)"
  while [[ $x == "" ]]; do
    echo "must provide $1"
    x="$(snapctl get $1)"
    sleep 1
  done
  CONFIG[$1]=$x
  echo "$1: $x"
}

require route-prefix
require index
require name
require cluster-subnet
require cluster-dns

# Containerd configuration

mkdir -p $SNAP_DATA/etc
mkdir -p $SNAP_COMMON/var/runc
cat > $SNAP_DATA/etc/containerd-config.toml <<EOF
[plugins]
  [plugins.cri.containerd]
    snapshotter = "overlayfs"
    [plugins.cri.containerd.default_runtime]
      runtime_type = "io.containerd.runtime.v1.linux"
      runtime_engine = "$SNAP/bin/runc"
      runtime_root = "$SNAP_COMMON/var/runc"
  [plugins.cri.cni]
    bin_dir = "$SNAP/bin/cni"
    conf_dir = "$SNAP_DATA/etc/cni/net.d"
EOF

# CNI configuration

mkdir -p $SNAP_DATA/etc/cni/net.d
cat > $SNAP_DATA/etc/cni/net.d/10-bridge.conf <<EOF
{
    "cniVersion": "0.3.1",
    "name": "bridge",
    "type": "bridge",
    "bridge": "cnio0",
    "isGateway": true,
    "ipMasq": true,
    "ipam": {
        "type": "host-local",
        "ranges": [
          [{"subnet": "${CONFIG[route-prefix]}.${CONFIG[index]}.0/24"}]
        ],
        "routes": [{"dst": "0.0.0.0/0"}]
    }
}
EOF
cat > $SNAP_DATA/etc/cni/net.d/99-loopback.conf <<EOF
{
    "cniVersion": "0.3.1",
    "name": "lo",
    "type": "loopback"
}
EOF

# kubelet

mkdir -p $SNAP_DATA/kubelet
cat > $SNAP_DATA/kubelet/kubelet-config.yaml <<EOF
kind: KubeletConfiguration
apiVersion: kubelet.config.k8s.io/v1beta1
authentication:
  anonymous:
    enabled: false
  webhook:
    enabled: true
  x509:
    clientCAFile: "$SNAP_DATA/keychain/ca.pem"
authorization:
  mode: Webhook
clusterDomain: "cluster.local"
clusterDNS:
  - "${CONFIG[cluster-dns]}"
podCIDR: "${CONFIG[route-prefix]}.${CONFIG[index]}.0/24"
runtimeRequestTimeout: "15m"
tlsCertFile: "$SNAP_DATA/keychain/${CONFIG[name]}.pem"
tlsPrivateKeyFile: "$SNAP_DATA/keychain/${CONFIG[name]}-key.pem"
EOF

# kubeconfig

$SNAP/bin/kubectl config set-cluster mergetb \
  --certificate-authority=$SNAP_DATA/keychain/ca.pem \
  --embed-certs=true \
  --server=https://px:6443 \
  --kubeconfig=${CONFIG[name]}.kubeconfig

$SNAP/bin/kubectl config set-credentials system:node:${CONFIG[name]} \
  --client-certificate=$SNAP_DATA/keychain/${CONFIG[name]}.pem \
  --client-key=$SNAP_DATA/keychain/${CONFIG[name]}-key.pem \
  --embed-certs=true \
  --kubeconfig=${CONFIG[name]}.kubeconfig

$SNAP/bin/kubectl config set-context default \
  --cluster=mergetb \
  --user=system:node:${CONFIG[name]} \
  --kubeconfig=${CONFIG[name]}.kubeconfig

$SNAP/bin/kubectl config use-context default \
  --kubeconfig=${CONFIG[name]}.kubeconfig

mkdir -p $SNAP_DATA/kubelet
mv ${CONFIG[name]}.kubeconfig $SNAP_DATA/kubelet/kubeconfig

# proxyconfig

$SNAP/bin/kubectl config set-cluster mergetb \
  --certificate-authority=$SNAP_DATA/keychain/ca.pem \
  --embed-certs=true \
  --server=https://ma0:6443 \
  --kubeconfig=kube-proxy.kubeconfig \

$SNAP/bin/kubectl config set-credentials kube-proxy \
  --client-certificate=$SNAP_DATA/keychain/kube-proxy.pem \
  --client-key=$SNAP_DATA/keychain/kube-proxy-key.pem \
  --embed-certs=true \
  --kubeconfig=kube-proxy.kubeconfig

$SNAP/bin/kubectl config set-context default \
  --cluster=mergetb \
  --user=kube-proxy \
  --kubeconfig=kube-proxy.kubeconfig

$SNAP/bin/kubectl config use-context default \
  --kubeconfig=kube-proxy.kubeconfig

mkdir -p $SNAP_DATA/kube-proxy
mv kube-proxy.kubeconfig $SNAP_DATA/kube-proxy/kubeconfig


# kubelete gets very angry without the following
swapoff -a

echo "configuration complete"
touch $SNAP_DATA/configured

# set up routes
#i=0
#while [ $i -lt {{N}} ]; do
#  next_hop=`dig +short kn$i.portal.{{domain}}`
#  if [[ $i != {{INDEX}} ]]; then
#    ip route add 10.74.$i.0/24 via $next_hop
#  fi
#	let i=i+1
#  sleep 1
#done

# for now leave this up to the pre-cluster setup
# set up routes
#for i in `seq 1 ${CONFIG[cluster-size]}`; do
#  if [[ $i != ${CONFIG[index]} ]]; then
#    ip route add ${CONFIG[route-prefix]}.$i.0/24 via ${CONFIG[node-prefix]}.$((20+i))
#  fi
#done
