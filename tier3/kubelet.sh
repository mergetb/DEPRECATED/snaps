#!/bin/bash

. $SNAP/bin/service.sh

mkdir -p $SNAP_COMMON/var/lib/kubelet

mkdir -p /run/merge-t3
export XDG_RUNTIME_DIR=/run/merge-t3

$SNAP/bin/kubelet \
  --cloud-provider= \
  --config=$SNAP_DATA/kubelet/kubelet-config.yaml \
  --container-runtime=remote \
  --container-runtime-endpoint=unix:///var/run/containerd/containerd.sock \
  --image-pull-progress-deadline=2m \
  --kubeconfig=$SNAP_DATA/kubelet/kubeconfig \
  --network-plugin=cni \
  --register-node=true \
  --root-dir $SNAP_COMMON/var/lib/kubelet \
  --v=3
  
#--bootstrap-checkpoint-path=$SNAP_COMMON/var/lib/kubelet \
