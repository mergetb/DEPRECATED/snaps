#!/bin/bash

set -e

docker build -t snap-builder .
docker run -v `pwd`:/stuff -w /stuff snap-builder ./snap.sh

# make sure user of script can access the snap.
docker run -v `pwd`:/stuff -w /stuff snap-builder /bin/bash -c "chown $UID ./*.snap && chmod go+r ./*.snap"
