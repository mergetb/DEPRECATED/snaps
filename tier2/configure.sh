#!/bin/bash

set -e

# Get required configuration parameters ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

declare -A CONFIG

require() {
  x="$(snapctl get $1)"
  while [[ $x == "" ]]; do
    echo "must provide $1"
    x="$(snapctl get $1)"
    sleep 1
  done
  CONFIG[$1]=$x
  echo "$1: $x"
}

require_file() {
  while [[ ! -f $1 ]]; do
    echo "must provide file: $1"
    sleep 1
  done
}

require_dir() {
  while [[ ! -d $1 ]]; do
    echo "must provide file: $1"
    sleep 1
  done
}
