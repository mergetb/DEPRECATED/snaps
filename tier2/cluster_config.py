#!/usr/bin/env python3

import yaml

with open('/snap/merge-t2/current/etc/rook/cluster_base.yml') as f:
    cluster = list(yaml.load_all(f))

with open('/var/snap/merge-t2/current/k8s/mycluster.yml') as f:
    my = yaml.load(f)

list(cluster)[10]['spec']['storage']['nodes'] = my['nodes']

print(yaml.dump_all(cluster, default_flow_style=False))
